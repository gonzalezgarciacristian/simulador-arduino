# coding: utf-8
from dataclasses import dataclass, field
from typing import List
import os

class Datos_programa:
    atributos = []
    funciones = []
    # lista en la que se incluiran todos los atributos declarados como variables globales,
    # ya que, si se quieren modificar, en Python es necesario usar la clausula 'global'
    variables_globales = []
    # indica si se están utilizando estructuras, para de este modo usar ctypes
    estructura_usada = False
    delay_usado = False

    def nueva_funcion(self, funcion):
        self.funciones.append(funcion)

    def nuevo_atributo(self, atrib):
        self.atributos.append(atrib)

    def nueva_variable_global(self, var):
        self.variables_globales.append(var)
    
    def usar_estructura(self):
        self.estructura_usada = True

    def usar_delay(self):
        self.delay_usado = True

datos_programa = Datos_programa()

# recorre el cuerpo de funcion, if, while...
def recorre_cuerpo(cuerpo, n, tab):
    resultado = ""
    for c in cuerpo:
        if isinstance(c, Expresion):
            resultado += f'{tab}\t{c.str(n+1)}\n'
            #resultado += tab + "\t" + c.str(n) + "\n"
        elif type(c) != list:
            resultado += f'{c.str(n+1)}\n'
            #resultado += c.str(n+1) + "\n"
        else:
            for elto in c:
                if isinstance(elto, Expresion):
                    resultado += f'{tab}\t{elto.str(n+1)}\n'
                    #resultado += tab + "\t" + elto.str(n) + "\n"
                else:
                    resultado += f'{elto.str(n+1)}\n'
                    #resultado += elto.str(n+1) + "\n"
    return resultado


@dataclass
class Nodo:
    linea: int
    def str(self, n):
        return f'{n*" "}#{self.linea}\n'

@dataclass
class Expresion(Nodo):
    def str(self, n):
        return

##############
# Return
##############

@dataclass
class Finalizador(Nodo):
    def str(self, n):
        return

@dataclass
class Return(Finalizador):
    devuelve: Expresion
    def str(self, n):
        resultado = "\t" * n
        resultado += f'return'
        if self.devuelve != None:
            resultado += f' {self.devuelve.str(n)}'
            #resultado += " " + self.devuelve.str(n)
        return resultado

@dataclass
class Break(Finalizador):
    def str(self, n):
        resultado = "\t" * n
        resultado += f'break'
        return resultado

@dataclass
class Continue(Finalizador):
    def str(self, n):
        resultado = "\t" * n
        resultado += f'continue'
        return resultado

##############
# Include
##############
@dataclass
class Include(Nodo):
    fichero: str
    def str(self, n):
        nombre_fichero = self.fichero[1:len(self.fichero) - 1]
        resultado = f'#include {nombre_fichero}'
        #resultado = "#include " + nombre_fichero
        return resultado

##############
# Constante
##############
@dataclass
class Constante(Nodo):
    tipo: str
    nombre: str
    valor: Expresion
    def str(self, n):
        resultado = "\t" * n
        resultado += f'{self.nombre} = {self.valor.str(n)}'
        #resultado += self.nombre + " = " + self.valor.str(n)
        return resultado

##############
# Atributos
##############
@dataclass
class Atributo(Nodo):
    tipo: str
    nombre: str
    valor: Expresion
    def str(self, n):
        resultado = "\t" * n
        resultado += f'{self.nombre} = '
        #resultado += self.nombre + " = "
        if self.valor != None:
            resultado += str(self.valor.str(n))
        elif self.tipo == "int" or self.tipo == "long"\
            or self.tipo == "short" or self.tipo == "byte":
            resultado += "0"
        elif self.tipo == "float" or self.tipo == "double":
            resultado += "0.0"
        elif self.tipo == "char":
            resultado += '""'
        elif self.tipo == "bool":
            resultado += "False"
        return resultado

@dataclass
class Array(Nodo):
    tipo: str
    nombre: str
    longitud: int
    elementos: List[Expresion]
    def str(self, n):
        resultado = "\t" * n
        if len(self.elementos) == 0:
            resultado += f'{self.nombre} = [None] * {self.longitud.str(n)}'
        else:
            resultado += f'{self.nombre} = ['
            for elto in self.elementos:
                resultado += f'{elto.str(n)}, '
            resultado = resultado[0:len(resultado) - 2]
            resultado += "]"
        return resultado

@dataclass
class Atributo_parametro(Nodo):
    tipo: str
    nombre: str
    def str(self, n):
        resultado = self.nombre
        return resultado

@dataclass
class Array_parametro(Nodo):
    tipo: str
    nombre: str
    longitud: int
    def str(self, n):
        resultado = self.nombre
        return resultado

@dataclass
class Estructura_parametro(Nodo):
    estructura: str
    nombre: str
    def str(self, n):
        resultado = self.nombre
        return resultado

@dataclass
class Estructura_array_parametro(Nodo):
    estructura: str
    nombre: str
    longitud: int
    def str(self, n):
        resultado = self.nombre
        return resultado

@dataclass
class Estructura(Nodo):
    nombre: str
    elementos: List
    def str(self, n):
        datos_programa.usar_estructura()
        resultado = "\t" * n
        resultado += f'class {self.nombre}(Structure):\n'
        resultado += f'\t_fields_ = [\n'
        for elemento in self.elementos:
            if type(elemento) != list:
                resultado += f'\t\t("{elemento.nombre}", c_{elemento.tipo}),\n'
                #resultado += '\t\t("' + elemento.nombre + '", ' + "c_" + elemento.tipo + "),\n"
            else:
                for i in elemento:
                    resultado += f'\t\t("{i.nombre}", c_{i.tipo}),\n'
                    #resultado += '\t\t("' + i.nombre + '", ' + "c_" + i.tipo + "),\n"
        resultado += f'\t]'
        return resultado

@dataclass
class Estructura_y_declaradas(Nodo):
    nombre: str
    elementos: List
    lista_estructuras: List
    def str(self, n):
        resultado = f'{Estructura(self.linea, self.nombre, self.elementos).str(n)}\n'
        #resultado = Estructura(self.linea, self.nombre, self.elementos).str + "\n"
        for estructura in self.lista_estructuras:
            resultado += f'{estructura.str(n)}\n'
            #resultado += estructura.str(n) + "\n"
        return resultado

@dataclass
class Estructura_atributo(Nodo):
    estructura: str
    nombre: str
    elementos: List[Expresion]
    def str(self, n):
        resultado = "\t" * n
        resultado += f'{self.nombre} = {self.estructura}'
        #resultado += self.nombre + " = " + self.estructura
        if self.elementos != None:
            resultado += "("
            for elemento in self.elementos:
                resultado += f'{elemento.str(n)}, '
                #resultado += elemento.str(n) + ","
            resultado += ")"
        else:
            resultado += "()"
        return resultado

@dataclass
class Estructura_array(Nodo):
    estructura: str
    nombre: str
    longitud: int
    elementos: List[Expresion]
    def str(self, n):
        resultado = "\t" * n
        resultado += f'{self.nombre} = [{self.estructura}()] * {str(self.longitud.str(n))}'
        #resultado += self.nombre + " = [" + self.estructura + "()] * " + str(self.longitud.str(n))     # Hay un caso en el que la longitud del array no se da
        return resultado                                                                     # Falta asignar los elementos

##############
# Funcion
##############
@dataclass
class Funcion(Nodo):
    tipo: str
    nombre: str
    parametros: List
    cuerpo: List
    return_statement: Finalizador
    def str(self, n):
        resultado = f'def {self.nombre}('
        #resultado = "def " + self.nombre + "("
        hay_parametros = False
        for p in self.parametros:
            resultado += f'{p.nombre}, '
            #resultado += p.nombre + ", "
            hay_parametros = True
        if hay_parametros:
            resultado = resultado[0:len(resultado) - 2]
        resultado += "):\n"
        if self.cuerpo == []:
            resultado += "\tpass\n"
        else:
            resultado2 = recorre_cuerpo(self.cuerpo, n, "")
            for var in datos_programa.variables_globales:
                if var in resultado2:
                    resultado += f'\tglobal {var}\n'
            resultado += resultado2
        if type(self.return_statement) != list:
            resultado += f'{self.return_statement.str(n+1)}\n'
        return resultado

##############
# If
##############
@dataclass
class Else_statement(Nodo):
    cuerpo: List
    return_statement: Finalizador
    def str(self, n):
        resultado = "\t" * n
        tab = "\t" * n
        resultado += "else:\n"
        resultado += recorre_cuerpo(self.cuerpo, n, tab)
        if type(self.return_statement) != list:
            resultado += f'{self.return_statement.str(n+1)}\n'
        return resultado

@dataclass
class Else_if(Nodo):
    condicion: Expresion
    cuerpo: List
    return_statement: Finalizador
    def str(self, n):
        resultado = "\t" * n
        tab = "\t" * n
        resultado += f'elif {str(self.condicion.str(n))}:\n'
        if "espera()" in resultado:
            resultado = resultado[0:resultado.index("espera()")] + resultado[resultado.index("espera()") + 8:len(resultado)]
            resultado = resultado.replace("\n", "")
            resultado = resultado.replace("\t", "")
            resultado = f'{tab}{resultado}\n{tab}\tespera()\n'
        #resultado += "elif " + str(self.condicion.str(n)) + ":\n"
        resultado += recorre_cuerpo(self.cuerpo, n, tab)
        if type(self.return_statement) != list:
            resultado += f'{self.return_statement.str(n+1)}\n'
            #resultado += self.return_statement.str(n+1) + "\n"
        return resultado

@dataclass
class If_statement(Nodo):
    condicion: Expresion
    cuerpo: List
    return_statement: Finalizador
    else_ifs: List[Else_if]
    else_statement: Else_statement
    def str(self, n):
        resultado = "\t" * n
        tab = "\t" * n
        # if
        resultado += f'if {str(self.condicion.str(n))}:\n'
        if "espera()" in resultado:
            resultado = resultado[0:resultado.index("espera()")] + resultado[resultado.index("espera()") + 8:len(resultado)]
            resultado = resultado.replace("\n", "")
            resultado = resultado.replace("\t", "")
            resultado = f'{tab}{resultado}\n{tab}\tespera()\n'
        #resultado += "if " + str(self.condicion.str(n)) + ":\n"
        resultado += recorre_cuerpo(self.cuerpo, n, tab)
        if type(self.return_statement) != list:
            resultado += f'{self.return_statement.str(n+1)}\n'
            #resultado += self.return_statement.str(n+1) + "\n"
        # elif
        for else_if in self.else_ifs:
            resultado += else_if.str(n)
        # else
        if type(self.else_statement) != list:
            resultado += self.else_statement.str(n)
        return resultado

##############
# Switch
##############
@dataclass
class Default_switch(Nodo):
    cuerpo: List
    return_statement: Finalizador
    def str(self, n):
        resultado = "\t" * n
        tab = "\t" * n
        resultado += "else:\n"
        resultado += recorre_cuerpo(self.cuerpo, n, tab)
        """if type(self.return_statement) != list:
            resultado += f'{self.return_statement.str(n+1)}\n'"""
        return resultado

@dataclass
class Case_switch(Nodo):
    condicion: Expresion
    cuerpo: List
    return_statement: Finalizador
    def str(self, n, condicion, caso_inicial):
        resultado = "\t" * n
        tab = "\t" * n
        if caso_inicial:
            resultado += f'if {str(self.condicion.str(n))} == {str(condicion.str(n))}:\n'
        else:
            resultado += f'elif {str(self.condicion.str(n))} == {str(condicion.str(n))}:\n'
        #resultado += "if " + str(self.condicion.str(n)) + " == " + str(condicion.str(n)) + ":\n"
        resultado += recorre_cuerpo(self.cuerpo, n, tab)
        # El finalizador sera un break en todo caso, no incluirlo
        """if type(self.return_statement) != list:
            resultado += f'{self.return_statement.str(n+1)}\n'"""
        return resultado

@dataclass
class Switch(Nodo):
    condicion: Expresion
    cases: List[Case_switch]
    default: Default_switch
    def str(self, n):
        resultado = ''
        for i, case in enumerate(self.cases):
            if i == 0:
                resultado += case.str(n, self.condicion, True)
            else:
                resultado += case.str(n, self.condicion, False)
        if type(self.default) != list:
            resultado += self.default.str(n)
        return resultado

##############
# While
##############
@dataclass
class While(Nodo):
    condicion: Expresion
    cuerpo: List
    return_statement: Finalizador
    def str(self, n):
        resultado = "\t" * n
        tab = "\t" * n
        resultado += f'while {str(self.condicion.str(n))}:\n'
        if "espera()" in resultado:
            resultado = resultado[0:resultado.index("espera()")] + resultado[resultado.index("espera()") + 8:len(resultado)]
            resultado = resultado.replace("\n", "")
            resultado = resultado.replace("\t", "")
            resultado = f'{tab}{resultado}\n{tab}\tespera()\n'
        #resultado += "while " + str(self.condicion.str(n)) + ":\n"
        resultado += recorre_cuerpo(self.cuerpo, n, tab)
        if type(self.return_statement) != list:
            resultado += f'{self.return_statement.str(n+1)}\n'
        return resultado

##############
# Do-while
##############
@dataclass
class Do_while(Nodo):
    condicion: Expresion                    # while True:
    cuerpo: List                            #   cuerpo
    return_statement: Finalizador           #   if condicion:
    def str(self, n):                       #       break
        resultado = "\t" * n
        tab = "\t" * n
        resultado += "while True:\n"
        num_linea = self.linea   # número de línea que corresponderá al if y al break
        for c in self.cuerpo:
            if isinstance(c, Expresion):
                resultado += f'{tab}\t{c.str(n)}\n'
                #resultado += tab + "\t" + c.str(n) + "\n"
                num_linea += 1
            elif type(c) != list:
                resultado += f'{c.str(n+1)}\n'
                #resultado += c.str(n+1) + "\n"
                num_linea += 1
            else:
                for elto in c:
                    if isinstance(elto, Expresion):
                        resultado += f'{tab}\t{elto.str(n)}\n'
                        #resultado += tab + "\t" + elto.str(n) + "\n"
                        num_linea += 1
                    else:
                        resultado += f'{elto.str(n+1)}\n'
                        #resultado += elto.str(n+1) + "\n"
                        num_linea += 1
        if type(self.return_statement) != list:
            resultado += f'{self.return_statement.str(n+1)}\n'
        if_statement = If_statement(num_linea, self.condicion, [Break(num_linea + 1)], [], [], [])
        resultado += if_statement.str(n+1)
        return resultado

##############
# For
##############
@dataclass
class For(Nodo):
    condicion_inicio: Expresion
    condicion_fin: Expresion
    condicion_incremento: Expresion
    cuerpo: List
    return_statement: Finalizador
    def str(self, n):
        resultado = "\t" * n
        tab = "\t" * n
        resultado += f'{self.condicion_inicio.str(n)}\n{tab}while {self.condicion_fin.str(0)}:\n'
        if "espera()" in resultado:
            resultado = resultado[0:resultado.index("espera()")] + resultado[resultado.index("espera()") + 8:len(resultado)]
            resultado = resultado.replace("\n", "")
            resultado = resultado.replace("\t", "")
            resultado = f'{tab}{resultado}\n{tab}\tespera()\n'
        # cuerpo del bucle
        #resultado += self.cuerpo.str(n+1)
        resultado += recorre_cuerpo(self.cuerpo, n, tab)
        resultado += f'{tab}\t{self.condicion_incremento.str(n+1)}'
        #resultado += self.condicion_incremento.str(n+1)
        return resultado

##############
# Expresiones
##############

@dataclass
class Prototipo_funcion(Expresion):
    tipo: str
    nombre: str
    parametros: List
    def str(self, n):
        resultado = f'# prototipo_funcion -> {self.tipo} {self.nombre}('
        #resultado = "# prototipo -> " + self.tipo + " " + self.nombre + "("
        hay_parametros = False
        for c in self.parametros:
            if type(c) != list:
                if type(c) == str:
                    resultado += f'{c}, '
                    #resultado += c + ", "
                else:
                    resultado += f'{c.str(n)}, '
                    #resultado += c.str(n) + ", "
            else:
                for elto in c:
                    if type(elto) == str:
                        resultado += f'{elto}, '
                        #resultado += elto + ", "
                    else:
                        resultado += f'{elto.str(n)}, '
                        #resultado += elto.str(n) + ", "
            hay_parametros = True
        if hay_parametros:
            resultado = resultado[0:len(resultado) - 2]
        resultado += ")\n"
        return resultado

@dataclass
class Llamada_funcion(Expresion):
    nombre: str
    parametros: List[Expresion]
    def str(self, n):
        if self.nombre == "printf":
            resultado = f'print({self.parametros[0].str(n)}'
            if len(self.parametros) > 1:
                resultado += f' % ('
                for p in self.parametros[1:]:
                    resultado += f'{p.str(n)}, '
                resultado = resultado[0:len(resultado) - 2]
                resultado += ")"
            resultado += ")"
        elif self.nombre == "delay":
            resultado = f'sleep({self.parametros[0].str(n)}/1000)'
            datos_programa.usar_delay()
        else:
            resultado = f'{self.nombre}('
            #resultado = self.nombre + "("
            hay_parametros = False
            parametros = ""
            for c in self.parametros:
                if type(c) != list:
                    parametros += f'{c.str(n)}, '
                    #resultado += c.str(n) + ", "
                else:
                    for elto in c:
                        parametros += f'{elto.str(n)}, '
                        #resultado += elto.str(n) + ", "
                hay_parametros = True
            if hay_parametros:
                parametros = parametros[0:len(parametros) - 2]
            resultado += f'{parametros})'
            if self.nombre == "lee_nodo":
                tab = "\t" * n
                resultado = f'{parametros} = {resultado}\n{tab}espera()'
            nombre = self.nombre
            if nombre == "prepara" or nombre == "sal_aqui" or nombre == "sal" or nombre == "siguiente" or \
                nombre == "lee_numero" or nombre == "luce_numero" or nombre == "error":
                tab = "\t" * n
                resultado = f'{resultado}\n{tab}espera()'
        return resultado

@dataclass
class Array_elemento(Expresion):
    nombre: str
    posicion: Expresion
    def str(self, n):
        resultado = f'{self.nombre}[{str(self.posicion.str(n))}]'
        #resultado = self.nombre + "[" + str(self.posicion.str(n)) + "]"
        return resultado

@dataclass
class Estructura_elemento(Expresion):
    nombre: str
    campo: str
    def str(self, n):
        resultado = f'{self.nombre}.{self.campo}'
        #resultado = self.nombre + "." + self.campo
        return resultado

@dataclass
class Estructura_array_elemento(Expresion):
    nombre: str
    posicion: Expresion
    campo: str
    def str(self, n):
        resultado = f'{self.nombre}[{str(self.posicion.str(n))}].{self.campo}'
        #resultado = self.nombre + "[" + str(self.posicion.str(n)) + "]." + self.campo
        return resultado

###############
# Op. Binaria
###############

@dataclass
class OperacionBinaria(Expresion):
    izquierda: Expresion
    derecha: Expresion

@dataclass
class Suma(OperacionBinaria):
    operador: str = '+'
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Resta(OperacionBinaria):
    operador: str = '-'
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Multiplicacion(OperacionBinaria):
    operador: str = '*'
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Division(OperacionBinaria):
    operador: str = '/'
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Resto(OperacionBinaria):
    operador: str = '%'
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Equal_to(OperacionBinaria):
    operador: str = '=='
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Not_equal_to(OperacionBinaria):
    operador: str = '!='
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Mayor(OperacionBinaria):
    operador: str = '>'
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Menor(OperacionBinaria):
    operador: str = '<'
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Mayor_igual(OperacionBinaria):
    operador: str = '>='
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Menor_igual(OperacionBinaria):
    operador: str = '<='
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class And_logico(OperacionBinaria):
    operador: str = '&&'
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} and {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " and " + str(self.derecha.str(n))
        return resultado

@dataclass
class Or_logico(OperacionBinaria):
    operador: str = '||'
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} or {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " or " + str(self.derecha.str(n))
        return resultado

@dataclass
class And_bitwise(OperacionBinaria):
    operador: str = '&'
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Or_bitwise(OperacionBinaria):   # exp | exp
    operador: str = '|'
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Or_bitwise_exclusive(OperacionBinaria):     # exp ^ exp
    operador: str = '^'
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Left_shift(OperacionBinaria):
    operador: str = '<<'
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Right_shift(OperacionBinaria):
    operador: str = '>>'
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

###############
# Op.Unaria
###############

@dataclass
class OperacionUnaria(Expresion):
    operando: Expresion

@dataclass
class Post_incremento(OperacionUnaria):
    operador: str = '++'
    def str(self, n):
        resultado = f'{self.operando.str(n)} = {self.operando.str(n)} + 1'
        #resultado = self.operando.str(n) + "=" +self.operando.str(n) + " + 1"
        return resultado

@dataclass
class Post_decremento(OperacionUnaria):
    operador: str = '--'
    def str(self, n):
        resultado = f'{self.operando.str(n)} = {self.operando.str(n)} - 1'
        #resultado = self.operando.str(n) + "=" +self.operando.str(n) + " - 1"
        return resultado

@dataclass
class Pre_incremento(OperacionUnaria):
    operador: str = '++'
    def str(self, n):
        resultado = f'{self.operando.str(n)} = {self.operando.str(n)} + 1'
        #resultado = self.operando.str(n) + "=" +self.operando.str(n) + " + 1"
        return resultado

@dataclass
class Pre_decremento(OperacionUnaria):
    operador: str = '--'
    def str(self, n):
        resultado = f'{self.operando.str(n)} = {self.operando.str(n)} - 1'
        #resultado = self.operando.str(n) + "=" +self.operando.str(n) + " - 1"
        return resultado

@dataclass
class Negativo(OperacionUnaria):
    operador: str = '-'
    def str(self, n):
        resultado = f'{self.operador}{str(self.operando.str(n))}'
        #resultado = self.operador + str(self.operando.str(n))
        return resultado

@dataclass
class Positivo(OperacionUnaria):
    operador: str = '+'
    def str(self, n):
        resultado = f'{self.operador}{str(self.operando.str(n))}'
        #resultado = self.operador + str(self.operando.str(n))
        return resultado

@dataclass
class Not(OperacionUnaria):
    operador: str = '!'
    def str(self, n):
        resultado = f'not {str(self.operando.str(n))}'
        #resultado = "not " + str(self.operando.str(n))
        #resultado = "not " + self.operando.str(n)
        return resultado

@dataclass
class Bitwise_complement(OperacionUnaria):       # ~ exp
    operador: str = '~'
    def str(self, n):
        resultado = f'{self.operador}{str(self.operando.str(n))}'
        #resultado = self.operador + str(self.operando.str(n))
        return resultado

###############
# Asignacion
###############

@dataclass
class Asignar(Expresion):
    izquierda: str
    derecha: str

@dataclass
class Asignacion(Asignar):
    operador: str = '='
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Asignacion_suma(Asignar):
    operador: str = '+='
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Asignacion_resta(Asignar):
    operador: str = '-='
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Asignacion_mult(Asignar):
    operador: str = '*='
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Asignacion_div(Asignar):
    operador: str = '/='
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Asignacion_resto(Asignar):
    operador: str = '%='
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Asignacion_left_shift(Asignar):
    operador: str = '<<='
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Asignacion_right_shift(Asignar):
    operador: str = '>>='
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Asignacion_and(Asignar):
    operador: str = '&='
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Asignacion_xor(Asignar):
    operador: str = '^='
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Asignacion_or(Asignar):
    operador: str = '|='
    def str(self, n):
        resultado = f'{str(self.izquierda.str(n))} {self.operador} {str(self.derecha.str(n))}'
        #resultado = str(self.izquierda.str(n)) + " " + self.operador + " " + str(self.derecha.str(n))
        return resultado

@dataclass
class Objeto(Expresion):
    nombre: str
    def str(self, n):
        resultado = self.nombre
        return resultado

@dataclass
class Int(Expresion):
    valor: int
    def str(self, n):
        resultado = str(self.valor)
        return resultado

@dataclass
class String(Expresion):
    valor: str
    def str(self, n):
        resultado = self.valor
        return resultado

@dataclass
class Char(Expresion):
    valor: str
    def str(self, n):
        resultado = self.valor
        return resultado

@dataclass
class Float(Expresion):
    valor: float
    def str(self, n):
        resultado = str(self.valor)
        return resultado

@dataclass
class Bool(Expresion):
    valor: bool
    def str(self, n):
        if str(self.valor) == "true":
            resultado = "True"
        else:
            resultado = "False"
        return resultado

##############
# Programa
##############
@dataclass
class Programa(Nodo):
    cuerpo: List
    def str(self, n = 0):
        resultado = ""
        # lista con las funciones del programa, esta se recorrera despues
        # para asi guardar las variables globales primero
        lista_funciones = []
        for elemento in self.cuerpo:
            if type(elemento) == Funcion:
                lista_funciones.append(elemento)
            elif type(elemento) != list:
                resultado += f'{elemento.str(n)}\n'
                #resultado += elemento.str(n) + "\n"
            else:
                for i in elemento:
                    resultado += f'{i.str(n)}\n'
                    if type(i) == Atributo:
                        datos_programa.nueva_variable_global(i.nombre)
                    #resultado += i.str(n) + "\n"
        for func in lista_funciones:
            resultado += f'{func.str(n)}\n'
        if datos_programa.estructura_usada:
            resultado = f'from ctypes import *\n{resultado}'
            #resultado = "from ctypes import *\n" + resultado
        if datos_programa.delay_usado:
            resultado = f'from time import sleep\n{resultado}'
        return resultado