#coding: utf-8
from Clases import *
from Lexer import C_Lexer
from sly import Parser
import llvmlite.binding as llvm
from ctypes import CFUNCTYPE
import sys
import os

CARPETA = os.path.join(r"C:\Users\Sergio\Desktop\SERGIO\Unican\4 - Cuarto\TFG" \
                       "\Compilador\grading")
FICHEROS = os.listdir(CARPETA)
TESTS = [fich for fich in FICHEROS
         if os.path.isfile(os.path.join(CARPETA, fich))
         and fich.endswith(".c")]
TESTS.sort()

# ficheros incluidos
included_files = []

class C_Parser(Parser):
    tokens = C_Lexer.tokens
    errores = []
    # no_heredables =

    # ordenar orecedencia de menos a más
    precedence = (
        ('left',MEMBER_SELECTION,'.'),    # faltan () de función y [] de array
        ('right',INCREMENT,DECREMENT,'!','~'),      # sizeof y (type) //Faltan + y - unarios y * Y &
        ('left','*','/','%'),
        ('left','+','-'),
        ('left',LEFT_SHIFT,RIGHT_SHIFT),
        ('left','<',LESS_EQUAL,'>',GREATER_EQUAL),
        ('left',EQUAL_TO,NOT_EQUAL_TO),
        ('left','&'),
        ('left','^'),
        ('left','|'),
        ('left',AND),
        ('left',OR),
        #('?:'),     # condicional ternario
        ('right','=',ASSIGN_SUM,ASSIGN_DIFFERENCE,ASSIGN_PRODUCT, \
             ASSIGN_QUOTIENT,ASSIGN_REMAINDER,ASSIGN_LEFT_SHIFT, \
             ASSIGN_RIGHT_SHIFT,ASSIGN_AND,ASSIGN_XOR,ASSIGN_OR),
        ('left',',')
    )

    # =============================================================================================
    # =============================================================================================
    # PROGRAMA ====================================================================================
    # =============================================================================================
    # =============================================================================================

    """@_('cuerpo_programa main_funcion cuerpo_programa')
    def programa(self, p):
        return Programa(0, p[0], p[1], p[2])"""

    @_('cuerpo_programa')
    def programa(self, p):
        return Programa(0, p[0])

    # cuerpo_programa
    @_('')
    def cuerpo_programa(self, p):
        return []

    @_('cuerpo_programa include')
    def cuerpo_programa(self, p):
        return p[0] + [p[1]]
    
    @_('cuerpo_programa define_const')
    def cuerpo_programa(self, p):
        return p[0] + [p[1]]

    @_('cuerpo_programa atributos_declarados')
    def cuerpo_programa(self, p):
        return p[0] + [p[1]]

    @_('cuerpo_programa grupo_atributos')
    def cuerpo_programa(self, p):
        return p[0] + [p[1]]

    @_('cuerpo_programa estructura')
    def cuerpo_programa(self, p):
        return p[0] + [p[1]]

    @_('cuerpo_programa prototipo_funcion')
    def cuerpo_programa(self, p):
        return p[0] + [p[1]]

    @_('cuerpo_programa funcion')
    def cuerpo_programa(self, p):
        return p[0] + [p[1]]

    # =============================================================================================
    # =============================================================================================
    # INCLUDES ====================================================================================
    # =============================================================================================
    # =============================================================================================

    @_('INCLUDE INCLUDED_FILE')
    def include(self, p):
        included_files.append(p[1][1:len(p[1]) - 1])
        return Include(p.lineno, p[1])

    # =============================================================================================
    # =============================================================================================
    # CONSTANTES ==================================================================================
    # =============================================================================================
    # =============================================================================================

    @_('CONST TYPE_ID OBJECT_ID "=" expresion ";"')
    def define_const(self, p):
        return Constante(p.lineno, p[1], p[2], p[4])

    @_('TYPE_ID CONST OBJECT_ID "=" expresion ";"')
    def define_const(self, p):
        return Constante(p.lineno, p[0], p[2], p[4])
    
    @_('DEFINE OBJECT_ID expresion')
    def define_const(self, p):
        return Constante(p.lineno, None, p[1], p[2])
    
    # prototipo función
    @_('TYPE_ID OBJECT_ID "(" lista_tipos ")" ";"')
    def prototipo_funcion(self, p):
        return Prototipo_funcion(p.lineno, p[0], p[1], p[3])

    @_('TYPE_ID OBJECT_ID "(" lista_atrib_parametros ")" ";"')
    def prototipo_funcion(self, p):
        return Prototipo_funcion(p.lineno, p[0], p[1], p[3])
    
    # =============================================================================================
    # =============================================================================================
    # ATRIBUTOS ===================================================================================
    # =============================================================================================
    # =============================================================================================

    # p.e: struct {int a, b; char c;}

    @_('')
    def lista_declarados(self, p):
        return []

    @_('lista_declarados atributos_declarados')
    def lista_declarados(self, p):
        return p[0] + [p[1]]
            
    # p.e: int a, b;

    @_('atrib_declarado')
    def lista_atrib_declarados(self, p):
        return [p[0]]

    @_('array_declarado')
    def lista_atrib_declarados(self, p):
        return [p[0]]

    @_('lista_atrib_declarados "," atrib_declarado')
    def lista_atrib_declarados(self, p):
        return p[0] + [p[2]]

    @_('lista_atrib_declarados "," array_declarado')
    def lista_atrib_declarados(self, p):
        return p[0] + [p[2]]

    @_('OBJECT_ID')
    def atrib_declarado(self, p):
        return Atributo(p.lineno, None, p[0], None)

    @_('TYPE_ID lista_atrib_declarados ";"')
    def atributos_declarados(self, p):
        for atrib in p[1]:
            atrib.tipo = p[0]
            """if atrib.tipo == 'int':
                atrib.valor = Int(atrib.linea, 0)
            elif atrib.tipo == 'float' or atrib.valor == 'double':
                atrib.valor = Float(atrib.linea, 0.0)"""
        return p[1]

    # p.e: int b = 5, c[8], d[] = {0,1,2}, e[8] = {0,1,2};

    @_('atrib_declarado')
    def lista_atrib(self, p):
        return [p[0]]
    
    @_('atrib_inicializado')
    def lista_atrib(self, p):
        return [p[0]]
    
    """@_('array_declarado')
    def lista_atrib(self, p):
        return [p[0]]"""
    
    @_('array_inicializado')
    def lista_atrib(self, p):
        return [p[0]]

    @_('lista_atrib "," atrib_declarado')
    def lista_atrib(self, p):
        return p[0] + [p[2]]

    @_('lista_atrib "," atrib_inicializado')
    def lista_atrib(self, p):
        return p[0] + [p[2]]

    @_('lista_atrib_declarados "," atrib_inicializado')
    def lista_atrib(self, p):
        return p[0] + [p[2]]

    """@_('lista_atrib "," array_declarado')
    def lista_atrib(self, p):
        return p[0] + [p[2]]

    @_('lista_atrib_declarados "," array_declarado')
    def lista_atrib(self, p):
        return p[0] + [p[2]]"""

    @_('lista_atrib "," array_inicializado')
    def lista_atrib(self, p):
        return p[0] + [p[2]]

    @_('lista_atrib_declarados "," array_inicializado')
    def lista_atrib(self, p):
        return p[0] + [p[2]]

    @_('OBJECT_ID "=" expresion')
    def atrib_inicializado(self, p):
        return Atributo(p.lineno, None, p[0], p[2])

    @_('OBJECT_ID "[" expresion "]"')
    def array_declarado(self, p):
        return Array(p.lineno, None, p[0], p[2], [])

    @_('OBJECT_ID "[" "]" "=" "{" lista_elementos "}"')
    def array_inicializado(self, p):
        return Array(p.lineno, None, p[0], len(p[5]), p[5])

    @_('OBJECT_ID "[" expresion "]" "=" "{" lista_elementos "}"')
    def array_inicializado(self, p):
        return Array(p.lineno, None, p[0], p[2], p[6])

    @_('TYPE_ID lista_atrib ";"')
    def grupo_atributos(self, p):
        for atrib in p[1]:
            atrib.tipo = p[0]
            """if type(atrib) == Atributo and atrib.valor == None:
                if atrib.tipo == 'int':
                    atrib.valor = Int(atrib.linea, 0)
                elif atrib.tipo == 'float' or atrib.tipo == 'double':
                    atrib.valor = Float(atrib.linea, 0.0)"""
            # Inicializar los valores del array
            #elif type(atrib) == Array:"""
        return p[1]

    # p.e: func(int a, char b, int c[x], struct Arduino d, struct Arduino e[y])

    @_('')
    def lista_atrib_parametros(self, p):
        return []

    @_('atrib_parametro')
    def lista_atrib_parametros(self, p):
        return [p[0]]

    @_('lista_atrib_parametros "," atrib_parametro')
    def lista_atrib_parametros(self, p):
        return p[0] + [p[2]]

    @_('TYPE_ID "*" OBJECT_ID')
    def atrib_parametro(self, p):
        return Atributo_parametro(p.lineno, p[0], p[2])

    @_('TYPE_ID "&" OBJECT_ID')
    def atrib_parametro(self, p):
        return Atributo_parametro(p.lineno, p[0], p[2])

    @_('TYPE_ID OBJECT_ID')
    def atrib_parametro(self, p):
        return Atributo_parametro(p.lineno, p[0], p[1])

    @_('TYPE_ID OBJECT_ID "[" expresion "]"')
    def atrib_parametro(self, p):
        return Array_parametro(p.lineno, p[0], p[1], p[3])

    @_('STRUCT OBJECT_ID OBJECT_ID')
    def atrib_parametro(self, p):
        return Estructura_parametro(p.lineno, p[1], p[2])

    @_('STRUCT OBJECT_ID OBJECT_ID "[" expresion "]"')
    def atrib_parametro(self, p):
        return Estructura_array_parametro(p.lineno, p[1], p[2], p[4])
        
    # struct Arduino {...};

    @_('STRUCT OBJECT_ID "{" lista_declarados "}" ";"')
    def estructura(self, p):
        return Estructura(p.lineno, p[1], p[3])
    
    # p.e: struct Arduino {int i} a, b;

    #@_('STRUCT OBJECT_ID "{" lista_declarados "}" lista_atrib_declarados ";"')
    @_('STRUCT OBJECT_ID "{" lista_declarados "}" lista_estructuras ";"')
    def estructura(self, p):
        for struct in p[5]:
            struct.estructura = p[1]
        #return Estructura(p.lineno, p[1], p[3]), p[5]
        return Estructura_y_declaradas(p.lineno, p[1], p[3], p[5])
    
    # p.e: struct Arduino a = {0,1,2}, b, c[8], d[] = {0,1}, e[8] = {{1,2},{3,4}};

    @_('struct_declarada')
    def lista_estructuras(self, p):
        return [p[0]]

    @_('struct_inicializada')
    def lista_estructuras(self, p):
        return [p[0]]

    @_('struct_array_declarado')
    def lista_estructuras(self, p):
        return [p[0]]

    @_('struct_array_inicializado')
    def lista_estructuras(self, p):
        return [p[0]]

    @_('lista_estructuras "," struct_declarada')
    def lista_estructuras(self, p):
        return p[0] + [p[1]]

    @_('lista_estructuras "," struct_inicializada')
    def lista_estructuras(self, p):
        return p[0] + [p[1]]

    @_('lista_estructuras "," struct_array_declarado')
    def lista_estructuras(self, p):
        return p[0] + [p[1]]

    @_('lista_estructuras "," struct_array_inicializado')
    def lista_estructuras(self, p):
        return p[0] + [p[1]]

    @_('OBJECT_ID')
    def struct_declarada(self, p):
        return Estructura_atributo(p.lineno, None, p[0], None)

    @_('OBJECT_ID "=" "{" lista_elementos "}"')
    def struct_inicializada(self, p):
        return Estructura_atributo(p.lineno, None, p[0], p[3])

    @_('OBJECT_ID "[" expresion "]"')
    def struct_array_declarado(self, p):
        return Estructura_array(p.lineno, None, p[0], p[2], None)

    @_('OBJECT_ID "[" "]" "=" "{" lista_elementos "}"')
    def struct_array_inicializado(self, p):
        return Estructura_array(p.lineno, None, p[0], len(p[5]), p[5])

    @_('OBJECT_ID "[" expresion "]" "=" "{" lista_elementos "}"')
    def struct_array_inicializado(self, p):
        return Estructura_array(p.lineno, None, p[0], p[2], p[6])

    @_('STRUCT OBJECT_ID lista_estructuras ";"')
    def estructura(self, p):
        for struct in p[2]:
            struct.estructura = p[1]
        return p[2]
    
    # =============================================================================================
    # =============================================================================================
    # FUNCIONES ===================================================================================
    # =============================================================================================
    # =============================================================================================

    @_('OBJECT_ID "(" lista_atrib_parametros ")" "{" cuerpo_funcion inst_fin "}"')
    def funcion(self, p):
        return Funcion(p.lineno, 'int', p[0], p[2], p[5], p[6])

    @_('TYPE_ID OBJECT_ID "(" lista_atrib_parametros ")" "{" cuerpo_funcion inst_fin "}"')
    def funcion(self, p):
        return Funcion(p.lineno, p[0], p[1], p[3], p[6], p[7])

    @_('OBJECT_ID "(" lista_atrib_declarados ")" "{" cuerpo_funcion inst_fin "}"')
    def funcion(self, p):
        for atrib in p[2]:
            atrib.tipo = 'int'
        return Funcion(p.lineno, 'int', p[0], p[2], p[5], p[6])

    @_('TYPE_ID OBJECT_ID "(" lista_atrib_declarados ")" "{" cuerpo_funcion inst_fin "}"')
    def funcion(self, p):
        for atrib in p[2]:
            atrib.tipo = 'int'
        return Funcion(p.lineno, p[0], p[1], p[3], p[6], p[7])

    # FUNCIÓN MAIN =================================================================================

    """@_('MAIN "(" lista_atrib_parametros ")" "{" cuerpo_funcion inst_fin "}"')
    def main_funcion(self, p):
        return Funcion(p.lineno, 'int', p[0], p[2], p[5], p[6])

    @_('TYPE_ID MAIN "(" lista_atrib_parametros ")" "{" cuerpo_funcion inst_fin "}"')
    def main_funcion(self, p):
        return Funcion(p.lineno, p[0], p[1], p[3], p[6], p[7])

    @_('MAIN "(" lista_atrib_declarados ")" "{" cuerpo_funcion inst_fin "}"')
    def main_funcion(self, p):
        for atrib in p[2]:
            atrib.tipo = 'int'
        return Funcion(p.lineno, 'int', p[0], p[2], p[5], p[6])

    @_('TYPE_ID MAIN "(" lista_atrib_declarados ")" "{" cuerpo_funcion inst_fin "}"')
    def main_funcion(self, p):
        for atrib in p[2]:
            atrib.tipo = 'int'
        return Funcion(p.lineno, p[0], p[1], p[3], p[6], p[7])"""

    # CUERPO FUNCIÓN (CONSTS, ATRIBS, EXPRS)
    @_('')
    def cuerpo_funcion(self, p):
        return []

    @_('cuerpo_funcion define_const')
    def cuerpo_funcion(self, p):
        return p[0] + [p[1]]

    @_('cuerpo_funcion atributos_declarados')
    def cuerpo_funcion(self, p):
        return p[0] + [p[1]]

    @_('cuerpo_funcion grupo_atributos')
    def cuerpo_funcion(self, p):
        return p[0] + [p[1]]

    @_('cuerpo_funcion estructura')
    def cuerpo_funcion(self, p):
        return p[0] + [p[1]]

    @_('cuerpo_funcion expresion ";"')
    def cuerpo_funcion(self, p):
        return p[0] + [p[1]]

    @_('cuerpo_funcion expresion_bloque')
    def cuerpo_funcion(self, p):
        return p[0] + [p[1]]

    # =============================================================================================
    # =============================================================================================
    # EXPRESIONES =================================================================================
    # =============================================================================================
    # =============================================================================================

    # if
    
    @_('IF "(" expresion ")" "{" cuerpo_funcion inst_fin "}" lista_else_if else_statement')
    def expresion_bloque(self, p):
        return If_statement(p.lineno, p[2], p[5], p[6], p[8], p[9])

    @_('')
    def else_statement(self, p):
        return []

    @_('ELSE "{" cuerpo_funcion inst_fin "}"')
    def else_statement(self, p):
        return Else_statement(p.lineno, p[2], p[3])

    @_('')
    def lista_else_if(self, p):
        return []

    @_('lista_else_if else_if')
    def lista_else_if(self, p):
        return p[0] + [p[1]]

    @_('ELSE IF "(" expresion ")" "{" cuerpo_funcion inst_fin "}"')
    def else_if(self, p):
        return Else_if(p.lineno, p[3], p[6], p[7])
    
    # switch

    @_('SWITCH "(" expresion ")" "{" lista_cases default_switch "}"')
    def expresion_bloque(self, p):
        return Switch(p.lineno, p[2], p[5], p[6])

    @_('')
    def lista_cases(self, p):
        return []

    @_('lista_cases case_switch')
    def lista_cases(self, p):
        return p[0] + [p[1]]

    @_('CASE expresion ":" cuerpo_funcion inst_fin')
    def case_switch(self, p):
        return Case_switch(p.lineno, p[1], p[3], p[4])

    """@_('CASE expresion ":" cuerpo_funcion BREAK ";"')
    def case_switch(self, p):
        return Case_switch(p.lineno, p[1], p[2], None)"""

    @_('')
    def default_switch(self, p):
        return []

    @_('DEFAULT ":" cuerpo_funcion inst_fin')
    def default_switch(self, p):
        return Default_switch(p.lineno, p[2], p[3])

    """@_('DEFAULT ":" cuerpo_funcion BREAK ";"')
    def default_switch(self, p):
        return Default_switch(p.lineno, p[2], None)"""
    
    # while

    @_('WHILE "(" expresion ")" "{" cuerpo_funcion inst_fin "}"')
    def expresion_bloque(self, p):
        return While(p.lineno, p[2], p[5], p[6])
    
    # do while
    @_('DO "{" cuerpo_funcion inst_fin "}" WHILE "(" expresion ")" ";"')
    def expresion_bloque(self, p):
        return Do_while(p.lineno, p[7], p[2], p[3])
    
    # for

    @_('FOR "(" expresion ";" expresion ";" expresion ")" "{" cuerpo_funcion inst_fin "}"')
    def expresion_bloque(self, p):
        return For(p.lineno, p[2], p[4], p[6], p[9], p[10])
    
    # prototipo función
    @_('TYPE_ID OBJECT_ID "(" lista_tipos ")"')         # sin ";", ya que deberían estar todas en 'lista exp ";"'
    def expresion(self, p):
        return Prototipo_funcion(p.lineno, p[0], p[1], p[3])

    @_('TYPE_ID OBJECT_ID "(" lista_atrib_parametros ")"')  # sin ";"
    def expresion(self, p):
        return Prototipo_funcion(p.lineno, p[0], p[1], p[3])

    @_('OBJECT_ID "(" lista_elementos ")"')
    def expresion(self, p): 
        return Llamada_funcion(p.lineno, p[0], p[2])

    @_('')
    def lista_tipos(self, p):
        return []

    @_('TYPE_ID')
    def lista_tipos(self, p):
        return [p[0]]

    @_('lista_tipos "," TYPE_ID')
    def lista_tipos(self, p):
        return p[0] + [p[2]]

    # arrays

    @_('OBJECT_ID "[" expresion "]"')   # quitado el ";" del final
    def expresion(self, p):
        return Array_elemento(p.lineno, p[0], p[2])

    # matrices
    """@_('TYPE_ID OBJECT_ID "[" expresion "]" "[" expresion "]" ";"')
    def expresion(self, p):
        return p

    @_('TYPE_ID OBJECT_ID "[" expresion "]" "[" expresion "]" "=" "{" lista_elementos "}" ";"')
    def expresion(self, p):
        return p"""
    
    # estructuras

    @_('OBJECT_ID "." OBJECT_ID')     # quitado el ["=" expresion]
    def expresion(self, p):
        return Estructura_elemento(p.lineno, p[0], p[2])

    @_('OBJECT_ID "[" expresion "]" "." OBJECT_ID')       # quitado el ["=" expresion]
    def expresion(self, p):
        return Estructura_array_elemento(p.lineno, p[0], p[2], p[5])

    @_('OBJECT_ID "." OBJECT_ID "[" expresion "]"')
    def expresion(self, p):
        return Estructura_elemento(p.lineno, p[0], Array_elemento(p.lineno, p[2], p[4]))

    # lista de elementos, p.e: 1,2,3,4
    @_('')
    def lista_elementos(self, p):
        return []

    @_('expresion')
    def lista_elementos(self, p):
        return [p[0]]

    @_('lista_elementos "," expresion')
    def lista_elementos(self, p):
        return p[0] + [p[2]]

    # operadores unarios y binarios
    @_('"(" expresion ")"')
    def expresion(self, p):
        return p[1]

    # cast
    """@_('"(" TYPE_ID ")" expresion')
    def expresion(self, p):
        return p"""

    # operadores aritméticos
    @_('expresion "+" expresion')
    def expresion(self, p):
        return Suma(p.lineno, p[0], p[2])

    @_('expresion "-" expresion')
    def expresion(self, p):
        return Resta(p.lineno, p[0], p[2])

    @_('expresion "*" expresion')
    def expresion(self, p):
        return Multiplicacion(p.lineno, p[0], p[2])

    @_('expresion "/" expresion')
    def expresion(self, p):
        return Division(p.lineno, p[0], p[2])

    @_('expresion "%" expresion')
    def expresion(self, p):
        return Resto(p.lineno, p[0], p[2])

    @_('expresion INCREMENT')
    def expresion(self, p):
        return Post_incremento(p.lineno, p[0])

    @_('expresion DECREMENT')
    def expresion(self, p):
        return Post_decremento(p.lineno, p[0])

    @_('INCREMENT expresion')
    def expresion(self, p):
        return Pre_incremento(p.lineno, p[1])

    @_('DECREMENT expresion')
    def expresion(self, p):
        return Pre_decremento(p.lineno, p[1])

    @_('"-" expresion')
    def expresion(self, p):
        return Negativo(p.lineno, p[1])

    @_('"+" expresion')
    def expresion(self, p):
        return Positivo(p.lineno, p[1])

    # operadores relacionales
    @_('expresion EQUAL_TO expresion')
    def expresion(self, p):
        return Equal_to(p.lineno, p[0], p[2])

    @_('expresion NOT_EQUAL_TO expresion')
    def expresion(self, p):
        return Not_equal_to(p.lineno, p[0], p[2])

    @_('expresion ">" expresion')
    def expresion(self, p):
        return Mayor(p.lineno, p[0], p[2])

    @_('expresion "<" expresion')
    def expresion(self, p):
        return Menor(p.lineno, p[0], p[2])

    @_('expresion GREATER_EQUAL expresion')
    def expresion(self, p):
        return Mayor_igual(p.lineno, p[0], p[2])

    @_('expresion LESS_EQUAL expresion')
    def expresion(self, p):
        return Menor_igual(p.lineno, p[0], p[2])

    # operadores lógicos
    @_('expresion AND expresion')
    def expresion(self, p):
        return And_logico(p.lineno, p[0], p[2])

    @_('expresion OR expresion')
    def expresion(self, p):
        return Or_logico(p.lineno, p[0], p[2])

    @_('"!" expresion')
    def expresion(self, p):
        return Not(p.lineno, p[1])

    # operadores bitwise
    @_('expresion "&" expresion')
    def expresion(self, p):
        return And_bitwise(p.lineno, p[0], p[2])

    @_('expresion "|" expresion')
    def expresion(self, p):
        return Or_bitwise(p.lineno, p[0], p[2])

    @_('expresion "^" expresion')
    def expresion(self, p):
        return Or_bitwise_exclusive(p.lineno, p[0], p[2])

    @_('"~" expresion')
    def expresion(self, p):
        return Bitwise_complement(p.lineno, p[1])

    @_('expresion LEFT_SHIFT expresion')
    def expresion(self, p):
        return Left_shift(p.lineno, p[0], p[2])

    @_('expresion RIGHT_SHIFT expresion')
    def expresion(self, p):
        return Right_shift(p.lineno, p[0], p[2])

    # operadores de asignación
    @_('expresion "=" expresion')
    def expresion(self, p):
        return Asignacion(p.lineno, p[0], p[2])

    @_('expresion ASSIGN_SUM expresion')
    def expresion(self, p):
        return Asignacion_suma(p.lineno, p[0], p[2])

    @_('expresion ASSIGN_DIFFERENCE expresion')
    def expresion(self, p):
        return Asignacion_resta(p.lineno, p[0], p[2])

    @_('expresion ASSIGN_PRODUCT expresion')
    def expresion(self, p):
        return Asignacion_mult(p.lineno, p[0], p[2])

    @_('expresion ASSIGN_QUOTIENT expresion')
    def expresion(self, p):
        return Asignacion_div(p.lineno, p[0], p[2])

    @_('expresion ASSIGN_REMAINDER expresion')
    def expresion(self, p):
        return Asignacion_resto(p.lineno, p[0], p[2])

    @_('expresion ASSIGN_LEFT_SHIFT expresion')
    def expresion(self, p):
        return Asignacion_left_shift(p.lineno, p[0], p[2])

    @_('expresion ASSIGN_RIGHT_SHIFT expresion')
    def expresion(self, p):
        return Asignacion_right_shift(p.lineno, p[0], p[2])

    @_('expresion ASSIGN_AND expresion')
    def expresion(self, p):
        return Asignacion_and(p.lineno, p[0], p[2])

    @_('expresion ASSIGN_XOR expresion')
    def expresion(self, p):
        return Asignacion_xor(p.lineno, p[0], p[2])

    @_('expresion ASSIGN_OR expresion')
    def expresion(self, p):
        return Asignacion_or(p.lineno, p[0], p[2])

    # operadores misceláneos (añadir sizeof, condicional ternario)
    @_('"&" expresion')
    def expresion(self, p):
        return p[1]

    @_('"*" expresion')
    def expresion(self, p):
        return p[1]

    # ID, int, string ...
    @_('OBJECT_ID')
    def expresion(self, p):
        return Objeto(p.lineno, p[0])

    @_('STRING_CONST')
    def expresion(self, p):
        return String(p.lineno, p[0])

    @_('FLOAT_CONST')
    def expresion(self, p):
        return Float(p.lineno, p[0])

    @_('INT_CONST')
    def expresion(self, p):
        return Int(p.lineno, p[0])

    @_('CHAR_CONST')
    def expresion(self, p):
        return Char(p.lineno, p[0])

    @_('BOOL_CONST')
    def expresion(self, p):
        return Bool(p.lineno, p[0])

    # instrucciones de finalización (continue, break, return)
    @_('')
    def inst_fin(self, p):
        return []

    # break
    @_('BREAK ";"')
    def inst_fin(self, p):
        return Break(p.lineno)

    # continue
    @_('CONTINUE ";"')
    def inst_fin(self, p):
        return Continue(p.lineno)

    # return

    @_('RETURN ";"')
    def inst_fin(self, p):
        return Return(p.lineno, None)

    @_('RETURN expresion ";"')
    def inst_fin(self, p):
        return Return(p.lineno, p[1])

    @_('RETURN "(" expresion ")" ";"')
    def inst_fin(self, p):
        return Return(p.lineno, p[2])


"""for i, fich in enumerate(TESTS):
    fich_entrada = open(os.path.join(CARPETA, fich), 'r')
    fich_salida = open(os.path.join(CARPETA, fich + '.pars'), 'w')"""
for i in range(1):
    fich_entrada = open(os.path.join(CARPETA, "Arduino.c"), 'r')
    fich_salida = open(os.path.join(CARPETA, "Arduino.c" + '.py'), 'w')
    print("-------------------------------------")
    print()
    print("Fichero: ", fich_entrada)
    lexer = C_Lexer()
    parser = C_Parser()
    entrada = fich_entrada.read()
    texto = parser.parse(lexer.tokenize(entrada))
    #print(texto)
    resultado = '\n'.join([c for c in texto.str(0).split('\n')])
    #print(resultado)
    print()
    fich_salida.write(resultado)
    fich_entrada.close()
    fich_salida.close()
    
